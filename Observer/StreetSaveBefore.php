<?php
namespace Linets\CustomerAddress\Observer;

use Magento\Framework\App\RequestInterface;

class StreetSaveBefore implements \Magento\Framework\Event\ObserverInterface
{
    private $_request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    ){
        $this->_request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $customerAddress =  $observer->getCustomerAddress();
        $post = $this->_request->getParams();
        $street[0] = $customerAddress->getStreet()[0];
        if (isset($post['street2'])){
            $street[1] = $post['street2'];
        }
        if (isset($post['street3'])){
            $street[2] = $post['street3'];
        }
        $customerAddress->setStreet($street);

        return $this;
    }
}